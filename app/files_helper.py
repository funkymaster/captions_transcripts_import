import os


def write_file(path, content, name):
    script_dir = get_script_dir()
    if not os.path.exists(os.path.join(script_dir, path)):
        os.mkdir(os.path.join(script_dir, path))
    f = open(os.path.join(script_dir, path, name), "w")
    f.write(content)
    f.close()


def read_file(path, name):
    script_dir = get_script_dir()
    f = open(os.path.join(script_dir, path, name), "r")
    content = f.read()
    f.close()
    return content


def get_file_path(path, file):
    script_dir = get_script_dir()
    if path:
        return os.path.join(script_dir, path, file)
    else:
        return os.path.join(script_dir, file)


def get_dir_path(path):
    script_dir = get_script_dir()
    return os.path.join(script_dir, path)


def get_script_dir():
    return os.path.dirname(os.path.dirname(__file__))

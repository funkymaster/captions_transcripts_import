from app.data_helper import *
from tkinter import *
import tkinter.scrolledtext as tkscrolled


def compare():
    vtt_data = get_vtt_data()
    txt_data = get_txt_data()
    db_data = get_db_data()
    csv_data = get_csv_data()
    if vtt_data and txt_data and db_data and csv_data:
        comparision = {
            'db_csv': data_text(csv_db_compare(db_data, csv_data)),
            'csv_db': data_text(csv_db_compare(csv_data, db_data)),
            'csv_vtt': data_text(list_files_compare(csv_data, vtt_data)),
            'vtt_csv': data_text(files_list_compare(vtt_data, csv_data)),
            'db_vtt': data_text(list_files_compare(db_data, vtt_data)),
            'vtt_db': data_text(files_list_compare(vtt_data, db_data)),
            'csv_txt': data_text(list_files_compare(csv_data, vtt_data)),
            'txt_csv': data_text(files_list_compare(txt_data, csv_data)),
            'db_txt': data_text(list_files_compare(db_data, txt_data)),
            'txt_db': data_text(files_list_compare(txt_data, db_data))
        }
        return comparision
    return False


def csv_db_compare(data_1, data_2):
    result = []
    for item_1 in data_1:
        match = 0
        for item_2 in data_2:
            if item_1['title'] == item_2['title'] and item_1['video'] == item_2['video']:
                match = 1
        if match == 0:
            result.append(item_1)
    return result


def list_files_compare(data, files):
    result = []
    files = array_cut_extention(files)
    for item in data:
        if item['video'][:-4] not in files:
            result.append(item)
    return result


def files_list_compare(files, data):
    result = []
    for item in files:
        match = 0
        for row in data:
            if row['video'][:-4] == item[:-4]:
                match = 1
        if match == 0:
            result.append(item)
    return result


def output_comparison_tk(data):
    if data:
        root = Tk()
        root.title("Comparision")

        width, height = 1024, 900
        scroll_text = tkscrolled.ScrolledText(width=width, height=height, wrap='word')

        for key, data in data.items():
            scroll_text.insert(1.0, "\n***************\n{} \n****************\n{}".format(key, data))
            scroll_text.pack(side=LEFT)

        root.mainloop()
    else:
        print("Data missing")


def output_comparison(data):
    if data:
        for key, data in data.items():
            print("\n***************\n{} \n****************\n{}".format(key, data))
    else:
        print("Data missing")


def array_cut_extention(target_array):
    result = []
    for item in target_array:
        result.append(item[:-4])
    return result


def data_text(data):
    i = 1
    text = ''
    for item in data:
        if isinstance(item, str):
            text += "{}: {}\n".format(i, item)
        elif isinstance(item, dict):
            text += "{}: {}\n".format(i, str(item))
        else:
            text += "{}: {}\n".format(i, ' '.join(item))
        i += 1
    return text
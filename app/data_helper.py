from .db_connect import connect_mysql
from .files_helper import *
import os
import csv
from xml.etree import ElementTree


def get_db_docs():
    query = ("SELECT document_id, MAX(version_number) FROM version "
             "WHERE content LIKE '%.m4v%' OR content LIKE '%.mp4%' GROUP BY document_id")
    cursor = connect_mysql(query)
    data = []
    for (document_id, version_number) in cursor:
        query_3 = ("SELECT title FROM document "
                   "WHERE id='{}'".format(document_id))
        cursor_3 = connect_mysql(query_3)
        title = cursor_3[0][0]
        query_2 = ("SELECT content FROM version "
                   "WHERE document_id='{}' AND version_number='{}'".format(document_id, version_number))
        cursor_2 = connect_mysql(query_2)
        content = cursor_2[0][0]
        data.append({
            'document_id': document_id,
            'title': title,
            'content': content,
            'version_number': version_number
        })
    return data


def get_xml_files():
    docs = get_db_docs()
    dir_name = get_xml_file_path()
    dir = get_dir_path(dir_name)
    if not os.path.exists(dir):
        os.mkdir(dir)
    for (item) in docs:
        write_file(dir_name, item['content'], "rosen_{}.xml".format(item['document_id']))


def get_db_data():
    docs = get_db_docs()
    i = 1
    db_list = []
    for (item) in docs:
        tree = ElementTree.fromstring(item['content'])
        for node in tree.iter('figure'):
            video_url = node.attrib.get('swf_url')
            if video_url and (video_url[-4:] == '.m4v' or video_url[-4:] == '.mp4'):
                video_url = video_url.split('/')
                video_url = video_url[-1]
                db_list.append({
                    'num': i,
                    'document_id': item['document_id'],
                    'title': item['title'].strip('\"').strip(),
                    'video': video_url.strip('\"').strip()
                })
                i += 1

    return db_list


def get_csv_data():
    file = os.environ.get('CSV_FILE')
    file = get_file_path('{}'.format(os.getenv('PATH')), file)
    if not os.path.isfile(file):
        print("File ", file, " doesn't exists")
        exit(0)
    data = []
    with open(file) as csv_data_file:
        csv_reader = csv.reader(csv_data_file)
        i = 0
        for row in csv_reader:
            if row:
                line = ','.join(row).split(';')
                if i == 0:
                    headers = []
                    for item in line:
                        if 'itle' in item:
                            headers.append('title')
                        elif 'ideo' in item:
                            headers.append('video')
                        else:
                            headers.append(item)
                else:
                    key = 0
                    data_item = {'num': i}
                    for item in line:
                        if item is list:
                            item = ''.join(item)
                        data_item[headers[key]] = item.strip('\"').strip()
                        key += 1
                    data.append(data_item)
            i += 1

    return data


def get_vtt_data():
    path = "{}/VTT".format(os.environ.get('PATH'))
    path = get_dir_path(path)
    if not os.path.exists(path):
        print("Dir ", path, " doesn't exists")
        exit(0)
    return os.listdir(path)


def get_txt_data():
    path = "{}/TXT".format(os.environ.get('PATH'))
    path = get_dir_path(path)
    if not os.path.exists(path):
        print("Dir ", path, " doesn't exists")
        exit(0)
    return os.listdir(path)


def get_xml_files_list():
    script_dir = get_script_dir()
    full_path = os.path.join(script_dir, get_xml_file_path())
    if os.path.exists(full_path):
        files = list(filter(lambda x: os.path.isfile(os.path.join(full_path, x)), os.listdir(full_path)))  # files only
        return files
    return False


def get_xml_file_path():
    return '{}_xml'.format(os.getenv('PATH'))

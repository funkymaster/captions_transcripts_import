import os
import mysql.connector
import config


def connect_mysql(query, update=False, sql_list=False):
    config_data = {
        'user': os.environ.get('DB_USER', 'root'),
        'password': os.environ.get('DB_PASS', '1'),
        'host': os.environ.get('DB_HOST', '127.0.0.1'),
        'database': os.environ.get('DB_NAME', 'rosen_dlit'),
        'raise_on_warnings': True
    }

    cnx = mysql.connector.connect(**config_data)
    cursor = cnx.cursor()
    result = False
    if not update:
        cursor.execute(query)
        result = cursor.fetchall()
    elif sql_list:
        cursor.execute(query, sql_list)
        result = cursor.lastrowid
    cursor.close()
    cnx.close()
    if result:
        return result

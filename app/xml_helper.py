from app.data_helper import *
import re
from xml.etree import ElementTree


def find_figure(content, url):
    content_array = []
    figure_item = False
    flag = False
    figure = False
    for line in content.splitlines():
        if re.search('<figure', line) and not re.search('!--<figure', line):
            flag = True
        if flag:
            if figure_item:
                figure_item = "{}\n{}".format(figure_item, line)
            else:
                figure_item = line
        if re.search('</figure', line) and figure_item and flag:
            flag = False
            if figure_item.find(url) is not -1:
                content_array.append(figure_item)
            figure_item = False
    for item in content_array:
        pattern = re.compile(r'(<figure.*>.*?</figure>)', re.DOTALL)
        figure = pattern.findall(item)
    if figure and len(content_array) == 1 and len(figure) == 1:
        return figure[0]
    else:
        return False


def get_subelement(node, elem):
    children = node.getchildren()
    for child in children:
        if child.tag == elem:
            return child
    return False


def prepare_content(content, filter=True):
    new_content = content
    vtt_data = get_vtt_data()
    txt_data = get_txt_data()
    parser = ElementTree.XMLParser(encoding="utf-8")
    tree = ElementTree.fromstring(content, parser=parser)
    for node in tree.iter('figure'):
        video_url = node.attrib.get('swf_url')
        if video_url and (video_url[-4:] == '.m4v' or video_url[-4:] == '.mp4'):
            c_url_old = node.attrib.get('caption_url')
            if not c_url_old:
                c_url = video_url.replace(' ', '_') \
                    .replace('m4v', 'vtt') \
                    .replace('mp4', 'vtt') \
                    .replace('/articles/videos/', '/video/') \
                    .replace('/articles/video/', '/video/') \
                    .replace('/video/', '/video/captions/') \
                    .replace('/videos/', '/video/captions/')
                vtt_file = re.search(r'((?<=captions\/)(.+\.vtt))+', c_url)
                if vtt_file and vtt_file.group(0):
                    vtt_file = vtt_file.group(0)
                    if vtt_file not in vtt_data:
                        vtt_file = vtt_file.replace('(', '_').replace(')', '_')
                        c_url = c_url.replace('(', '_').replace(')', '_')
                    if vtt_file in vtt_data:
                        node.set('caption_url', c_url)
                    else:
                        vtt_file = vtt_file.replace('(', '_').replace(')', '_')
                        print(' Missed file :  {}\n'.format(vtt_file))
            txt_file = re.search(r'(((?<=video\/)|(?<=videos\/))(.+\.(m4v|mp4)))+', video_url)
            swf_url = False
            if txt_file and txt_file.group(0):
                swf_url = txt_file.group(0)
                txt_file = "{}.txt".format(txt_file.group(0)[:-4])
            if txt_file in txt_data:
                with open(get_file_path("{}/TXT".format(os.environ.get('PATH')), txt_file), mode='rt') as t_file:
                    txt_content = t_file.read()
                    transcript = get_subelement(node, 'transcript')
                    if transcript is False:
                        node.makeelement('transcript', {})
                        ElementTree.SubElement(node, 'transcript', {})
                    transcript = get_subelement(node, 'transcript')
                    transcript.text = txt_content
                t_file.close()
            else:
                print('Missed file : {}\n'.format(txt_file))
            node_content = ElementTree.tostring(node, encoding='unicode')
            if swf_url:
                old_content = find_figure(new_content, swf_url)
                if old_content:
                    new_content = new_content.replace(old_content, node_content)
    if new_content == content and filter:
        return False
    else:
        return new_content


def prepare_xml_files(id=False):
    xml_files = get_xml_files_list()
    if not xml_files:
        print("Dir ", xml_files_new_path(), " doesn't exists")
        exit(0)
    script_dir = get_script_dir()
    new_path = os.path.join(script_dir, xml_files_new_path())
    if not os.path.exists(new_path):
        os.mkdir(new_path)
    vtt_data = get_vtt_data()
    txt_data = get_txt_data()
    if not vtt_data or not txt_data:
        print("Data captions or transcripts missing")
        exit(0)
    if id:
        xml_files = ['rosen_{}.xml'.format(id)]
    for xml_f in xml_files:
        content = read_file(get_xml_file_path(), xml_f)
        new_content = prepare_content(content, False)
        if new_content:
            write_file(xml_files_new_path(), new_content, xml_f)
        else:
            print('File not created/updated - {}'.format(xml_f))


def xml_files_new_path():
    return "{}_xml_new".format(os.getenv('PATH'))

#!/usr/bin/python
from app.xml_helper import *
from app.compare_helper import *
import csv
import os
import datetime


def create_video_import_file(file_name):
    site_url = os.environ.get('SITE_URL', 'https://digitalliteracy.rosendigital.com')
    if os.path.isfile(file_name):
        csv_data = get_csv_data()
        with open(file_name, mode='w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            csv_writer.writerow(['sourceurl', 'title', 'tags', 'sourcetype', 'sourceformat'])
            for item in csv_data:
                csv_writer.writerow([
                    "{}/staticfiles/video/{}".format(site_url, item['video']),
                    "Diglit: {}".format(item['title']),
                    'pkess',
                    'url',
                    'mp4'
                ])


def input_text():
    text = 'Enter number:\n'\
            '0 - Exit\n'\
            '1 - Get xml files\n'\
            '2 - Compare sources\n'\
            '3 - Create file for input video\n'\
            '4 - Prepare xml files\n'\
            '5 - Insert xml in DB\n'\
            '6 - Prepare one xml file\n'
    return text


def add_xml_item_in_db(document_id, version_number, content, event, last_modified):
    sql_list = (content, last_modified, '1', event, version_number, document_id)
    sql_query = "INSERT INTO `version` (`content`, `last_modified`, `modified_by_id`, `event`, `version_number`, `document_id`) VALUES (%s, %s, %s, %s, %s, %s)"
    version_id = connect_mysql(sql_query, True, sql_list)
    sql_list = (version_id, document_id)
    sql_query = "UPDATE `document` SET `current_version_id`=%s WHERE `id`=%s"
    connect_mysql(sql_query, True, sql_list)


def add_xml_in_db():
    db_docs = get_db_docs()
    for item in db_docs:
        version_number = item['version_number'] + 1
        document_id = item['document_id']
        content = item['content']
        event = "edited"
        new_content = prepare_content(content)
        if new_content:
            content = new_content
            today = datetime.date.today()
            last_modified = today.strftime("%Y-%m-%d %H:%M:%S")
            add_xml_item_in_db(document_id, version_number, content, event, last_modified)
            print('{} doc added'.format(document_id))
        else:
            print('Failed {}'.format(document_id))


def user_input():
    input_number = input(input_text())
    if input_number == '1':
        get_xml_files()
    elif input_number == '2':
        output_comparison(compare())
    elif input_number == '3':
        create_video_import_file('video_import_file.csv')
    elif input_number == '4':
        prepare_xml_files()
    elif input_number == '5':
        add_xml_in_db()
    elif input_number == '6':
        id = input('Enter document ID ')
        prepare_xml_files(id)
    elif input_number == '0':
        exit(0)
    user_input()


def main():
    user_input()


if __name__ == '__main__':
    main()
